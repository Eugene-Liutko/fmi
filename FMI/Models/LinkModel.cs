﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FMI.Models
{
    public class LinkModel
    {
        public string Title { get; set; }
        public string URL { get; set; }
        public string Description { get; set; }
        public string PageTitle { get; set; }
    }
}
